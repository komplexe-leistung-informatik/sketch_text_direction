// Befehlsbibliothek für das LCD
#include <LiquidCrystal.h>

// Initialisieren der Ports des LCD 
//    D5 (Datenbus)
//    D4 (Datenbus)
//    R/W (Read or Write)
//    RS (Bestimmung ob Datenbit als Befehl oder Zeichendaten interpretiert werden)
//    Vee (Kontrastregelung)
//    Vdd (Stromversorgung))
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Variablen für Stringauswahl bei Scrolling nach links und rechts
int stringStartLeft  = 16;
int stringLengthLeft =  0;
int stringStartRight = -1;
int stringLengthRight = -1;

// Indiktor in welche Richtung gescrollt wird
// wird bei erstem Durchlauf nicht beachtet
// danach wird für abwechselndes Scrollen gesorgt
int indicator = random(2);

void setup() 
{
  // Groesse des Displays initialisieren
  lcd.begin(16, 2);
}

void loop() 
{
  // Cursor steuern (1. Stelle, 1. Zeile)
  // Textausgabe
  // Cursor steuern (1. Stelle, 2. Zeile)
  lcd.setCursor(0, 0);
  lcd.print("Projekt 4:");
  lcd.setCursor(0, 1);

  // Scrollauswahl, Indikator als Auswahlvariable
  if(indicator == 0)
  {
    // Text mit Scrolling-Funktion nach links ausgegeben
    lcd.print(scrollLcdLeft("Ich bin ein Text fuer das Textdirection-Projekt"));

    // 400ms warten
    delay(400);
  }
  else if(indicator == 1)
  {
    // Text mit Scrolling-Funktion nach rechts ausgegeben
    lcd.print(scrollLcdRight("Ich bin ein Text fuer das Textdirection-Projekt"));
    
    // 400ms warten
    delay(400); 
  } 
}

// Scroll-Funktion nach links
String scrollLcdLeft(String displayString)
{
  // Variable, die zurueckgegeben  wird
  String endString;

  // 16 Leerzeichen am Anfang und Ende des zusammegefassten Strings
  String scrollStringBegin = "                ";
  String scrollStringEnd   = "                ";

  // zusammengefasster String; 2x 16 Leerzeichen, dazwischen der gewuenschte Text
  String stringScroll = scrollStringBegin + displayString + scrollStringEnd;

  // zurueckgegebener String
  // bestehend aus einem Substring des zusammengefassten Strings mit den Auswahlvariablen
  endString = stringScroll.substring(stringStartLeft, stringLengthLeft);

  // Erhoehung der Auswahlvariablen um 1
  stringStartLeft++;
  stringLengthLeft++;
  
  // Abbruchbedingung - Grenze ist die Laenge des zusammengefassten Strings
  // Indikator wird für abwechselndes Scrollen auf 1 gesetzt
  if(stringStartLeft > stringScroll.length())
  {
    // Auswahlvariablen werden auf voreingestellte Werte gesetzt
    stringStartLeft = 16;
    stringLengthLeft = 0;
    
    indicator = 1;
  }

  // fertiger String wird zurueckgegeben
  return endString;
}

// Scroll-Funktion nach rechts
String scrollLcdRight(String displayString)
{
  // Variable, die zurueckgegeben  wird
  String endString;

  // 16 Leerzeichen am Anfang und Ende des zusammengefassten Strings
  String scrollStringBegin = "                ";
  String scrollStringEnd   = "                ";

  // zusammengefasster String; 2x 16 Leerzeichen, dazwischen der gewuenschte Text
  String stringScroll = scrollStringBegin + displayString + scrollStringEnd;
  endString = stringScroll.substring(stringStartRight, stringLengthRight);
  
  // Senkung der Auswahlvariablen um 1
  stringStartRight--;
  stringLengthRight--;

  // Abbruchbedingung - Grenze ist < 1
  // Indikator wird für abwechselndes Scrollen auf 0 gesetzt
  if(stringLengthRight < 1)
  {
    // Auswahlvariablen werden auf voreingestellte Werte gesetzt
    stringStartRight = stringScroll.length();
    stringLengthRight = stringStartRight - 16;
    
    indicator = 0;
  }

  // fertiger String wird zurueckgegeben
  return endString;
}
